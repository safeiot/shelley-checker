from unittest.mock import MagicMock, patch

# decorators
device = MagicMock()
callback = MagicMock()
action = MagicMock()
raise_event = MagicMock()
program = MagicMock()
interface = MagicMock()