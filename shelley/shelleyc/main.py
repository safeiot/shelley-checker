from shelley.shelleyc import parser


def main() -> None:
    parser.parse()


if __name__ == "__main__":
    main()
